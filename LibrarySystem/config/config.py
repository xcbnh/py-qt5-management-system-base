# -*- coding:utf-8 -*-
"""
全局配置文件，提供常量信息
"""
import os
import logging
import sys

module_logger = logging.getLogger("logger")


class Config():
    """
    @ClassName：Config
    @Description： 本项目只创建一个 Config 实例
    负责常用属性配置，包括窗体颜色，文件路径，字体等
    @Author：锦沐Python
    D:\A-XHSPro\PyQt5Pro\LibrarySystem
    """
    _instance = None  # 类变量，用于存储唯一实例

    # 主函数启动的绝对路径
    CURRENT_PATH = os.path.abspath('.')

    try:
        CURRENT_PATH = sys._MEIPASS  # pyinstaller打包后的路径
    except AttributeError:
        CURRENT_PATH = os.path.abspath(".")  # 当前工作目录的路径

    ICON_PATH = os.path.join(CURRENT_PATH, "views\\rcc\\icon.ico")
    # 图书封面保存文件夹
    BOOK_IMG_SAVE_DIR = "upload_imgs"
    # 统计图保存文件夹
    STATS_IMG_SAVE_DIR = "stats_imgs"
    # 图书图片保存路径
    BOOK_IMG_SAVE_PATH = os.path.join(CURRENT_PATH, BOOK_IMG_SAVE_DIR)
    # 统计图片路径
    STATS_IMG_SAVE_PATH = os.path.join(CURRENT_PATH, STATS_IMG_SAVE_DIR)
    # 数据持久化文件路径（json）
    DATA_JSON_PATH = os.path.join(CURRENT_PATH, "repositories\\json\\data.json")
    # 字体文件路径
    FONT_PATH = os.path.join(CURRENT_PATH, "views\\fonts\\Alibaba-PuHuiTi-Regular.ttf")

    @staticmethod
    def create_directory(directory):
        if not os.path.exists(directory):
            os.makedirs(directory)
            module_logger.info(f"创建文件夹{directory}")
        else:
            module_logger.info(f"文件夹{directory}已存在")

    def __new__(cls):
        # __new__ 方法确保只创建一个实例
        if not cls._instance:
            cls._instance = super().__new__(cls)
            cls.create_directory(cls.BOOK_IMG_SAVE_PATH)
            cls.create_directory(cls.STATS_IMG_SAVE_PATH)
        return cls._instance


