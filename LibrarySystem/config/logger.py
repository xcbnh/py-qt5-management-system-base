"""
@FileName：logger.py\n
@Description：\n
@Author：锦沐Python\n
@Time：2024/7/21 20:47\n
"""
import logging.config

# 日志配置，名称为 logger 项目只使用 logger
logger = logging.getLogger("logger")
logger.setLevel(level=logging.INFO)
handler = logging.FileHandler("log.txt")
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(filename)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(console)
