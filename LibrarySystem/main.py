# -*- coding:utf-8 -*-
"""
@FileName：main.py
@Description：主函数，创建主窗口并显示
@Author：锦沐Python
@Time：2024/7/21 18:23
"""

import sys

from PyQt5.QtWidgets import QApplication

from LibrarySystem.config.config import Config
from LibrarySystem.views.main_window import MainWindow

import LibrarySystem.config.logger        # 日志配置，勿删
import LibrarySystem.views.rcc.my_rcc_rc  # 导入UI资源，勿删


if __name__ == "__main__":
    Config()
    app = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec_())


