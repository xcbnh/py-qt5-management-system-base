"""
@FileName：clear_data.py\n
@Description：\n
@Author：锦沐Python\n
@Time：2024/7/26 9:22\n
"""
import datetime
import hashlib
import json
import random
import string
from datetime import datetime
import numpy as np
import pandas as pd

#
# def create_id(length):
#     """
#     生成唯一Id
#     :param input_password:
#     :return:
#     """
#     random_string = ''.join(random.choices(string.ascii_letters + string.digits, k=5))
#     unique_id = hashlib.md5(random_string.encode()).hexdigest()
#
#     return unique_id[:length]
#
#
# data = pd.read_csv("中文图书数据集.csv")
# data = data[["书名","作者","出版社","中国图书分类号","出版年月"]]
#
#
# # 生成日期范围
# start_date = '2005-04-05'
# end_date = '2024-07-08'
#
# # 创建日期范围的序列
# date_range = pd.date_range(start=start_date, end=end_date)
#
# # 将日期序列格式化为统一的时间格式
# formatted_dates = date_range.strftime('%Y-%m-%d').tolist()
#
# random.shuffle(formatted_dates)
# print(formatted_dates[:10])
# other = {
#         "ID":[create_id(10) for _ in range(5000) ],
#         "录入时间":formatted_dates[:5000],
#         "ISBN":[str(900000000+i) for i in range(5000) ],
#         "借阅状态":[create_id(5) for _ in range(5000) ],
#         "可借天数":[random.randint(10,30)  for _ in range(5000) ]
# }
#
# # 将其他数据添加到 data 的对应列中
# data["ID"] = other["ID"]
# data["录入时间"] = other["录入时间"]
# data["ISBN"] = other["ISBN"]
# data["借阅状态"] = other["借阅状态"]
# data["可借天数"] = other["可借天数"]
#
# # 打印前几行检查结果
# print(data.head())


# columns_to_keep = ['书名', '作者', '出版社', '中国图书分类号', '出版年月']
# data = data[columns_to_keep + ['ID', '录入时间', 'ISBN', '借阅状态', '可借天数']]

data = pd.read_csv("data.csv")

book_list = []

# 抽取数据的比例，例如抽取 50%
sampled_data = data.sample(frac=0.5)
# 将抽样数据与原始数据按行拼接
combined_data = pd.concat([data, sampled_data], ignore_index=True)

for index, book in combined_data.iterrows():
    print(book)
    b = {
        "book_id": book["ID"],
        "book_name": book["书名"],
        "ISBN":book["ISBN"],
        "record_time": datetime.now().strftime("%Y.%m.%d %H:%M:%S"),
        "category": book["中国图书分类号"],
        "author": book["作者"],
        "publisher":book["出版社"],
        "publication_date": book["出版年月"],
        "borrowed_by":"",
        "borrow_time": "",
        "loan_period": book["可借天数"],
        "due_time": "",
        "img_url": "upload_imgs\\1721902002.935139.png"
     }

    book_list.append(b)

user = {
            "user_id": "123456",
            "user_name": "123456",
            "email": "1234@123.com",
            "role": "user",
            "password": "e10adc3949ba59abbe56e057f20f883e",
            "account_status": 0,
            "registration_time": ""
        }
# 将书籍列表写入 JSON 文件
with open("../json/data.json", "w", encoding="utf-8") as file:
    json.dump({"users":[user],"books":book_list}, file, ensure_ascii=False, indent=4)