-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--  
-- Host: localhost    Database: library_system  
-- ------------------------------------------------------  
-- Server version	8.0.23  
  
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;  
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;  
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;  
/*!50503 SET NAMES utf8mb4 */;  
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;  
/*!40103 SET TIME_ZONE='+00:00' */;  
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;  
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;  
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;  
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;  
  
--  
-- Table structure for table `users`  
--  
  
DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `student_id` varchar(50) NOT NULL COMMENT '学生ID',
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `email` varchar(255) NOT NULL COMMENT '电子邮箱',
  `role` enum('admin', 'user') NOT NULL COMMENT '角色（管理员或用户）',
  `password` varchar(32) NOT NULL COMMENT '密码（假设为MD5加密）',
  `account_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '账户状态',
  `registration_time` datetime NOT NULL COMMENT '注册时间',
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户表';


DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `books` (
  `book_id` varchar(255) NOT NULL COMMENT '书籍ID',
  `title` varchar(255) NOT NULL COMMENT '书籍标题',
  `isbn` varchar(20) NOT NULL COMMENT 'ISBN号',
  `entry_time` datetime NOT NULL COMMENT '录入时间',
  `category` enum('literature', 'engineering', 'foreign_literature') NOT NULL COMMENT '书籍类别',
  `author` varchar(255) NOT NULL COMMENT '作者',
  `publisher` varchar(255) NOT NULL COMMENT '出版社',
  `publication_date` date NOT NULL COMMENT '出版日期',
  `borrowed_by` varchar(255) DEFAULT NULL COMMENT '借阅者（与users表的student_id相对应）',
  `borrow_time` datetime DEFAULT NULL COMMENT '借阅时间',
  `borrow_duration` int(11) NOT NULL DEFAULT '30' COMMENT '借阅时长（天数）',
  `due_time` datetime GENERATED ALWAYS AS (`borrow_time` + INTERVAL `borrow_duration` DAY) STORED COMMENT '应还时间',
  PRIMARY KEY (`book_id`),
  KEY `idx_borrowed_by` (`borrowed_by`),
  CONSTRAINT `books_ibfk_1` FOREIGN KEY (`borrowed_by`) REFERENCES `users` (`student_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='书籍表';
  

  

