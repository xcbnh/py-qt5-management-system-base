"""
@FileName：RepositoryHelper.py\n
@Description：\n
@Author：锦沐Python\n
@Time：2024/7/22 9:43\n
"""

from LibrarySystem.repositories.json.user_repository import UserRepository
from LibrarySystem.repositories.json.book_repository import BookRepository


class RepositoryHelper:
    """
    @ClassName：RepositoryHelper
    @Description：单例模式， 统一管理数据层接口实例
    负责创建唯一数据接口实例，提供给 业务层 访问
    @Author：锦沐Python
     """
    _user_repository = None
    _book_repository = None

    @classmethod
    def get_user_repository(cls):
        if cls._user_repository is None:
            cls._user_repository = UserRepository()
        return cls._user_repository

    @classmethod
    def get_book_repository(cls):
        if cls._book_repository is None:
            cls._book_repository = BookRepository()
        return cls._book_repository
