# -*- coding:utf-8 -*-
"""
book表实现增删改查
"""
import json
import os
import random
import logging

from LibrarySystem.config.config import Config
from LibrarySystem.repositories.json.interface.book_data_interface import BookDataInterface
from LibrarySystem.repositories.json.models.book_models import Book

module_logger = logging.getLogger("logger")


class BookRepository(BookDataInterface):
    """
    @ClassName：BookRepository
    @Description： 继承图书数据层接口类
    负责实现接口功能
    @Author：锦沐Python
    """

    def __init__(self):
        self.config = Config()
        self.file_name = self.config.DATA_JSON_PATH
        self._is_file_exists()

    def _read_file(self):
        with open(self.file_name, 'r', encoding='utf-8') as file_obj:
            data_json = json.load(file_obj)
            return data_json

    def _write_file(self, data_json):
        # 写入数据
        with open(self.file_name, 'w', encoding='utf-8', buffering=1024*1024) as file_obj:
            json.dump(data_json, file_obj, indent=4)

    def add_book(self, book: Book):
        """
        添加书籍
        :param book:
        :return:
        """
        book_id = book.book_id
        data_to_write = book.obj_to_dict()

        if data_to_write is None:
            module_logger.info("书籍数据为空")
            return (False, f"书籍数据为空")

        try:
            # 读数据
            data_json = self._read_file()
            # 遍历查找
            for b in data_json["books"]:
                if b["book_id"] == book_id:
                    module_logger.info("books 已存在")
                    return (False, f"books {book_id} 已存在")

            data_json["books"].append(data_to_write)
            # 写入数据
            self._write_file(data_json)

            return (True, f"books {book_id} 添加成功")
        except Exception as e:
            # 处理其他可能的异常
            module_logger.info(f"添加书籍失败：{e}")
            return (False, f"添加书籍失败：{e}")

    def update_book(self, book: Book):
        """
        根据book_id更新书籍信息
        :param book:
        :return:
        """
        book_id = book.book_id

        try:
            # 读数据
            found = False
            data_json = self._read_file()

        # 遍历查找
            for index, book_ in enumerate(data_json["books"]):
                if book_["book_id"] == book_id:
                    data_json["books"][index] = book.obj_to_dict()
                    found = True
                    break
            # 写入数据
            if not found:
                return (False, f"书籍{book_id} 不存在，无法更新")

            # 写入数据
            self._write_file(data_json)

            return (True, f"书籍{book_id} 更新成功")
        except Exception as e:
            # 处理其他可能的异常
            module_logger.info(f"修改书籍信息失败：{e}")
            return (False, f"修改书籍信息失败：{e}")

    def query_book(self, book_id: str):
        """
            查询书籍
            :param book_id:
            :return:
        """
        try:
            # 读数据
            data_json = self._read_file()
            # 遍历查找
            for index, book in enumerate(data_json["books"]):
                if book["book_id"] == book_id:
                    book_data = data_json["books"][index]
                    # 创建一个 Book 对象
                    get_book = Book(
                        book_id=book_data.get("book_id", ""),
                        book_name=book_data.get("book_name", ""),
                        ISBN=book_data.get("ISBN", ""),
                        record_time=book_data.get("record_time", ""),  # 或者使用其他默认值
                        category=book_data.get("category", ""),
                        author=book_data.get("author", ""),
                        publisher=book_data.get("publisher", ""),
                        publication_date=book_data.get("publication_date", ""),
                        borrowed_by=book_data.get("borrowed_by", ""),
                        borrow_time=book_data.get("borrow_time", ""),
                        loan_period=book_data.get("loan_period", 15),
                        due_time=book_data.get("due_time", ""),  # 实际应用中可能需要计算
                        img_url=book_data.get("img_url", "None")
                    )
                    return (True, get_book)
            return (False, f"书籍：{book_id} 不存在")
        except Exception as e:
            # 处理其他可能的异常
            module_logger.info(f"查询书籍信息失败：{e}")
            return (False, f"查询书籍信息失败：{e}")

    def del_book(self, book_id: str):
        """
        删除书籍
        :param book_id:
        :return:
        """
        try:
            # 读数据
            found = False

            data_json = self._read_file()
            # 遍历查找
            for index, book in enumerate(data_json["books"]):
                if book["book_id"] == book_id:
                    del data_json["books"][index]
                    found = True
                    break

            if not found:
                return (False, f"书籍{book_id} 不存在，无法删除")

            # 写入数据
            self._write_file(data_json)

            return (True, f"书籍{book_id} 删除成功")

        except Exception as e:
            # 处理其他可能的异常
            module_logger.info(f"删除书籍信息失败：{e}")
            return (False, f"删除书籍信息失败：{e}")

    def get_books(self, count: int):
        """
        :param count:  小于 0 表示获取全部
        :return:
        """
        book_list = []
        try:
            # 读数据
            data_json = self._read_file()
            for book_data in data_json["books"]:
                data = Book(
                    book_id=book_data.get("book_id", ""),
                    book_name=book_data.get("book_name", ""),
                    ISBN=book_data.get("ISBN", ""),
                    record_time=book_data.get("record_time", ""),  # 或者使用其他默认值
                    category=book_data.get("category", ""),
                    author=book_data.get("author", ""),
                    publisher=book_data.get("publisher", ""),
                    publication_date=book_data.get("publication_date", ""),
                    borrowed_by=book_data.get("borrowed_by", ""),
                    borrow_time=book_data.get("borrow_time", ""),
                    loan_period=book_data.get("loan_period", 15),
                    due_time=book_data.get("due_time", ""),
                    img_url=book_data.get("img_url", "")
                )
                book_list.append(data)
            # 顺序打乱，制造随机性展示
            random.shuffle(book_list)

            if count > 0:
                book_list = book_list[:count]
            return (True, book_list)

        except Exception as e:
            module_logger.info(f"Failed to get books: {e}")
            return (False, f"获取书籍失败：{e}")

    def _is_file_exists(self):
        """
        # 检查文件是否存在
        """
        if not os.path.exists(self.file_name):
            # 文件不存在，尝试创建文件
            try:
                # 使用'a+'模式打开文件，如果文件不存在则创建
                with open(self.file_name, 'a+'):
                    pass  # 这里不需要写入任何内容，只是创建文件
                module_logger.info(f"文件{self.file_name}已创建。")
            except Exception as e:
                # 处理在创建文件时可能发生的异常
                module_logger.info(f"创建文件{self.file_name}时发生错误：{e}")
        else:
            # 文件已经存在
            module_logger.info(f"文件{self.file_name}已存在。")
