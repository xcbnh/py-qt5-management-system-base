# -*- coding:utf-8 -*-

from abc import ABC, abstractmethod
from LibrarySystem.repositories.json.models.book_models import Book


class BookDataInterface(ABC):
    """
    @ClassName：BookDataInterface
    @Description： 图书数据层接口定义
    包含增删改查功能
    @Author：锦沐Python
    """
    @abstractmethod
    def add_book(self, book: Book):
        pass

    @abstractmethod
    def update_book(self, book: Book):
        pass

    @abstractmethod
    def query_book(self, book_id: str):
        pass

    @abstractmethod
    def del_book(self, book_id: str):
        pass

    @abstractmethod
    def get_books(self, count: int):
        pass
