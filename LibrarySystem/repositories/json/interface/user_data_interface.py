# -*- coding:utf-8 -*-
"""用户表的增删改查接口
"""
from abc import ABC, abstractmethod
from LibrarySystem.repositories.json.models.user_models import User


class UserDataInterface(ABC):
    """
    @ClassName：UserDataInterface
    @Description： 用户数据层接口定义
    包含增删改查功能
    @Author：锦沐Python
    """
    @abstractmethod
    def add_user(self, user: User):
        pass

    @abstractmethod
    def update_user(self, user: User):
        pass

    @abstractmethod
    def query_user(self, user_id: str):
        pass

    @abstractmethod
    def del_user(self, user_id: str):
        pass

    @abstractmethod
    def get_users(self, count: int):
        pass
