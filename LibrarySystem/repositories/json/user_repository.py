# -*- coding:utf-8 -*-
"""
user表实现增删改查
"""
import os
import json
import logging

from LibrarySystem.config.config import Config
from LibrarySystem.repositories.json.interface.user_data_interface import UserDataInterface
from LibrarySystem.repositories.json.models.user_models import User

module_logger = logging.getLogger("logger")


class UserRepository(UserDataInterface):
    """
    @ClassName：UserRepository
    @Description： 继承用户数据层接口类，本项目只创建一个实例
    负责实现接口功能
    @Author：锦沐Python
    """
    def __init__(self):
        self.config = Config()
        self.file_name = self.config.DATA_JSON_PATH
        self._is_file_exists()
    
    def _read_file(self):
        with open(self.file_name, 'r', encoding='utf-8') as file_obj:
            data_json = json.load(file_obj)
            return data_json

    def _write_file(self, data_json):
        # 写入数据
        with open(self.file_name, 'w', encoding='utf-8', buffering=1024*1024) as file_obj:
            json.dump(data_json, file_obj, indent=4)

    def add_user(self, user: User):
        """
         增加用户到数据库
        :param user:
        :return: (操作结果标志, 数据|提示信息)
        """
        user_id = user.user_id
        data_to_write = user.obj_to_dict()

        if data_to_write is None:
            module_logger.info("用户数据为空")
            return (False, "用户数据为空")

        try:
            data_json = self._read_file()
            # 遍历查找
            for user_ in data_json["users"]:
                if user_["user_id"] == user_id:
                    module_logger.info("users 已存在")
                    return (False, f"用户 {user_id} 已存在")

            data_json["users"].append(data_to_write)
            # 写入数据
            self._write_file(data_json)
            
            return (True, f"用户 {user_id} 添加成功")
        except Exception as e:
            # 处理其他可能的异常
            module_logger.info(f"添加用户失败：{e}")
            return (False, f"添加用户失败 {user_id}： {e}")

    def update_user(self, user: User):
        """
        根据user_id更新用户信息
        :param user:
        :return: (操作结果标志, 数据|提示信息)
        """
        user_id = user.user_id
        data_to_write = user.obj_to_dict()
        if data_to_write is None:
            return (False, f"用户数据为空，更新失败")
        try:
            # 读数据
            found = False
            data_json = self._read_file()
            # 遍历查找
            for index, u in enumerate(data_json["users"]):
                if u["user_id"] == user_id:
                    data_json["users"][index] = data_to_write
                    found = True
                    break

            if not found:
                return (False, f"用户不存在，更新失败")
            
            # 写入数据
            self._write_file(data_json)
            
            return (True, f"用户 {user_id} 更新成功")
        except Exception as e:
            # 处理其他可能的异常
            module_logger.info(f"修改用户信息失败：{e}")
            return (False, f"修改用户信息失败：{e}")

    def query_user(self, user_id: str):
        """
        查询用户
        :param user_id:
        :return: (操作结果标志, 数据|提示信息)
        """
        try:
            # 读数据
            data_json = self._read_file()
            # 遍历查找
            for index, u in enumerate(data_json["users"]):
                if u["user_id"] == user_id:
                    user_data = data_json["users"][index]
                    # 创建一个 User 对象
                    get_user = User(
                        user_id=user_data.get("user_id", "default"),
                        user_name=user_data.get("user_name", "default"),
                        email=user_data.get("email", "default"),
                        role=user_data.get("role", "user"),
                        password=user_data.get("password", ""),
                        account_status=user_data.get("account_status", 0),
                        create_time=user_data.get("create_time", "")
                    )
                    return (True, get_user)

            return (False, f"用户{user_id}不存在")
        
        except Exception as e:
            # 处理其他可能的异常
            module_logger.info(f"查询用户信息失败：{e}")
            return (False, f"查询用户信息失败：{e}")

    def del_user(self, user_id: str):
        """
        删除用户信息
        :param user_id:
        :return: (操作结果标志, 数据|提示信息)
        """
        try:
            # 读数据
            found = False
            data_json = self._read_file()
            # 遍历查找
            for index, u in enumerate(data_json["users"]):
                if u["user_id"] == user_id:
                    del data_json["users"][index]
                    found = True
                    break

            if not found:
                return (False, f"用户{user_id} 不存在，无法删除")

            # 写入数据
            self._write_file(data_json)
            
            return (True, f"用户{user_id} 已删除")

        except Exception as e:
            # 处理其他可能的异常
            module_logger.info(f"删除用户信息失败：{e}")
            return (False, f"删除用户信息失败：{e}")

    def get_users(self, count: int):
        """
        获取用户，如果小于0获取全部
        :param count:
        :return: (操作结果标志, 数据|提示信息)
        """
        user_list = []
        try:
            data_json = self._read_file()
            # 遍历查找
            for user_data in data_json["users"]:
                user = User(
                        user_id=user_data.get("user_id", "default"),
                        user_name=user_data.get("user_name", "default"),
                        email=user_data.get("email", "default"),
                        role=user_data.get("role", "user"),
                        password=user_data.get("password", ""),
                        account_status=user_data.get("account_status", 0),
                        create_time=user_data.get("create_time", "")
                    )
                user_list.append(user)

            if count > 0:
                user_list = user_list[:count]
                
            return (True, user_list)

        except Exception as e:
            module_logger.info(f"Failed to get users: {e}")
            return (False, f"获取用户失败：{e}")

    def _is_file_exists(self):
        """
        # 检查文件是否存在
        """
        if not os.path.exists(self.file_name):
            # 文件不存在，尝试创建文件
            try:
                # 使用'a+'模式打开文件，如果文件不存在则创建
                with open(self.file_name, 'a+'):
                    pass  # 这里不需要写入任何内容，只是创建文件
                module_logger.info(f"文件{self.file_name}已创建。")
            except Exception as e:
                # 处理在创建文件时可能发生的异常
                module_logger.info(f"创建文件{self.file_name}时发生错误：{e}")

        else:
            # 文件已经存在
            module_logger.info(f"db文件{self.file_name}已存在。")
