# -*- coding:utf-8 -*-
"""
@FileName：ServicesHelper.py\n
@Description：业务层帮助类，单例模式，所有业务接口从这访问
@Author：锦沐Python\n
@Time：2024/7/22 9:52\n
"""

from LibrarySystem.services.user_service import UserService
from LibrarySystem.services.update_service import UpdateService
from LibrarySystem.services.stats_service import StatsService
from LibrarySystem.services.record_service import RecordService
from LibrarySystem.services.query_service import QueryService
from LibrarySystem.services.role_service import RoleManager


class ServicesHelper:
    """
    @ClassName：ServicesHelper
    @Description：单例模式， 统一管理 业务层接口 实例
    负责创建唯一业务接口实例，提供给 GUI层 访问
    @Author：锦沐Python
     """
    _user_service = None
    _update_service = None
    _stats_service = None
    _record_service = None
    _query_service = None
    _role_service = None

    @classmethod
    def get_user_service(cls):
        if not cls._user_service:
            cls._user_service = UserService()
        return cls._user_service

    @classmethod
    def get_update_service(cls):
        if not cls._update_service:
            cls._update_service = UpdateService()
        return cls._update_service

    @classmethod
    def get_stats_service(cls):
        if not cls._stats_service:
            cls._stats_service = StatsService()
        return cls._stats_service

    @classmethod
    def get_record_service(cls):
        if not cls._record_service:
            cls._record_service = RecordService()
        return cls._record_service

    @classmethod
    def get_query_service(cls):
        if not cls._query_service:
            cls._query_service = QueryService()
        return cls._query_service

    @classmethod
    def get_role_service(cls):
        if not cls._role_service:
            cls._role_service = RoleManager()
        return cls._role_service
