# -*- coding:utf-8 -*-
"""
业务层 查询接口
"""
from abc import ABC, abstractmethod
from typing import List
from LibrarySystem.repositories.json.models.book_models import Book


class QueryServiceInterface(ABC):
    @abstractmethod
    def get_books_by_count(self, count: int):
        pass

    @abstractmethod
    def get_books_by_keywords(self, accept_data_func, key_words: str, user_id: str):
        pass

    @abstractmethod
    def borrow_book(self, accept_data_func, book_id: str, user_id: str):
        pass

    @abstractmethod
    def back_book(self, accept_data_func, book_id: str, user_id: str):
        pass

    @abstractmethod
    def sort_books(self, books: List[Book], sort_type: str):
        pass
