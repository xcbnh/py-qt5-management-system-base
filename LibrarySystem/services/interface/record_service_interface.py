# -*- coding:utf-8 -*-
"""
业务层 记录接口
"""
from abc import ABC, abstractmethod
from LibrarySystem.repositories.json.models.book_models import Book


class RecordServiceInterface(ABC):

    @abstractmethod
    def record_book(self, book: Book):
        pass
