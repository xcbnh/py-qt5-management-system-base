# -*- coding:utf-8 -*-
"""
@FileName：role_service_interface.py.py\n
@Description：角色管理接口
@Author：锦沐Python\n
@Time：2024/7/26 21:27\n
"""
from abc import ABC, abstractmethod

from LibrarySystem.repositories.json.models.user_models import User


class RoleServiceInterface(ABC):

    @abstractmethod
    def get_current_role_permissions(self):
        pass

    @abstractmethod
    def set_current_role(self,  user: User):
        pass

    @abstractmethod
    def check_permissions(self, permission: str):
        pass

    @abstractmethod
    def get_current_user(self):
        pass

    @abstractmethod
    def get_current_role(self):
        pass

    @abstractmethod
    def get_roles(self):
        pass
