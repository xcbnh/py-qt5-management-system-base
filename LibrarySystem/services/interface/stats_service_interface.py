# -*- coding:utf-8 -*-
"""
数据统计接口
"""
from abc import ABC, abstractmethod


class StatsServiceInterface(ABC):

        @abstractmethod
        def calculate_match_case_by_keywords_thresd(self, accept_data_func, keywords: str):
            pass

        @abstractmethod
        def calculate_user_case_thresd(self, accept_data_func, user_id: str):
            pass

        @abstractmethod
        def calculate_admin_case_thresd(self, accept_data_func):
            pass

        @abstractmethod
        def _set_calculate_tag(self):
            pass
