# -*- coding:utf-8 -*-
"""
业务层 更新接口
"""
from abc import ABC, abstractmethod

from LibrarySystem.repositories.json.models.book_models import Book


class UpdateServiceInterface(ABC):

    @abstractmethod
    def get_book_by_id(self, book_id: str):
        pass

    @abstractmethod
    def del_book_by_id(self, book_id: str):
        pass

    @abstractmethod
    def update_book_by_id(self, book: Book):
        pass
