# -*- coding:utf-8 -*-
"""
业务层 用户接口
"""
from abc import ABC, abstractmethod

from LibrarySystem.repositories.json.models.user_models import User


class UserServiceInterface(ABC):

    @abstractmethod
    def update_user_by_id(self, user: User):
        pass

    @abstractmethod
    def del_user_by_id(self, user_id: str):
        pass

    @abstractmethod
    def add_user(self, user: User):
        pass

    @abstractmethod
    def user_password_verification(self, user_id: str, password: str):
        pass
