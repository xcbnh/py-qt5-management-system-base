# -*- coding:utf-8 -*-
"""
查询图书
"""
import copy
import random
import logging
from datetime import datetime, timedelta
from typing import List

from PyQt5.QtCore import QObject, pyqtSignal, QThread

from LibrarySystem.repositories.json.models.book_models import Book
from LibrarySystem.repositories.json.RepositoryHelper import RepositoryHelper

from LibrarySystem.services.interface.query_service_interface import QueryServiceInterface

module_logger = logging.getLogger("logger")


class QueryService(QueryServiceInterface):
    """
    @ClassName：QueryService
    @Description： 继承 查询业务层 接口类，本项目只创建一个实例
    负责实现对书籍信息的查询功能
    该类大部分功能均使用多线程
    @Author：锦沐Python
    """

    def __init__(self):
        # 线程运行标识，阻止短时间内多次触发和创建线程，为 True 时允许启动线程
        self.calculate_1_tag = True
        self.thread_1 = None
        self.worker_1 = None

        self.calculate_2_tag = True
        self.thread_2 = None
        self.worker_2 = None

        self.calculate_3_tag = True
        self.thread_3 = None
        self.worker_3 = None

    def get_books_by_count(self, count: int):
        """
        根据数量获取书籍，count < 0 表示获取所有
        @param count:
        @return:
        """
        (flag, books) = RepositoryHelper.get_book_repository().get_books(count)
        return (flag, books)

    def get_books_by_keywords(self, accept_data_func, keywords: str, user_id: str):
        """
        根据关键词获取书籍
        @param accept_data_func: 槽函数
        @param keywords: 关键词
        @param user_id: 用户id
        """
        if self.calculate_1_tag:
            module_logger.info("线程1启动")
            self.calculate_1_tag = False
            self.thread_1 = QThread()
            self.worker_1 = Worker(keywords=keywords, user_id=user_id)
            self.worker_1.moveToThread(self.thread_1)

            # 连接耗时操作槽函数
            self.thread_1.started.connect(self.worker_1.calculate_books_by_keywords)
            self.worker_1.data_signal.connect(accept_data_func)
            self.worker_1.finally_signal.connect(self._set_calculate_tag)
            self.thread_1.start()
        else:
            module_logger.info("线程1锁定，稍后再试")

    def borrow_book(self, accept_data_func, book_id: str, user_id: str):
        """
        借阅书籍，成功则将借阅信息写入数据信息
        @param accept_data_func:
        @param book_id:
        @param user_id:
        """
        if self.calculate_2_tag:
            module_logger.info("线程2启动")
            self.calculate_2_tag = False
            self.thread_2 = QThread()
            self.worker_2 = Worker(user_id=user_id, book_id=book_id)
            self.worker_2.moveToThread(self.thread_2)

            # 连接耗时操函数
            self.thread_2.started.connect(self.worker_2.calculate_borrow_book)
            self.worker_2.data_signal.connect(accept_data_func)
            self.worker_2.finally_signal.connect(self._set_calculate_tag)
            self.thread_2.start()
        else:
            module_logger.info("线程2锁定，稍后再试")

    def back_book(self, accept_data_func, book_id: str, user_id: str):
        """
         归还书籍，成功则将借阅信息写入数据信息
        @param accept_data_func:
        @param book_id:
        @param user_id:
        """
        if self.calculate_3_tag:
            module_logger.info("线程3启动")
            self.calculate_3_tag = False
            self.thread_3 = QThread()
            self.worker_3 = Worker(user_id=user_id, book_id=book_id)
            self.worker_3.moveToThread(self.thread_3)

            # 连接耗时槽函数
            self.thread_3.started.connect(self.worker_3.calculate_back_book)
            self.worker_3.data_signal.connect(accept_data_func)
            self.worker_3.finally_signal.connect(self._set_calculate_tag)
            self.thread_3.start()
        else:
            module_logger.info("线程2锁定，稍后再试")

    def sort_books(self, books: List[Book], type: str):
        """
        更具 最新上架时间，热度，数量，借阅状态进行筛选
        :param books:
        :param type: hot_up | hot_down | record_date_up | num_up | num_down | borrowed | un_borrowed
        """
        # 中文到英文的映射字典
        chinese_to_english = {
            "热度升序": "hot_up",
            "热度降序": "hot_down",
            "记录日期升序": "record_date_up",
            "数量升序": "num_up",
            "数量降序": "num_down",
            "已借出的": "borrowed",
            "未借出的": "un_borrowed"
        }

        sort_type = chinese_to_english[type]

        if sort_type == "hot_up" or sort_type == "hot_down":
            # 使用字典来统计每个 borrowed_counts 的数量，并同时存储对应的书籍列表
            borrowed_counts = {}
            for book in books:
                isbn = book.ISBN
                if book.borrowed_by and isbn in borrowed_counts:
                    borrowed_counts[isbn][0] += 1
                    borrowed_counts[isbn][1] = book
                else:
                    borrowed_counts[isbn] = [1, book]

            # 根据数量对ISBN进行排序（升序）
            if sort_type == "hot_up":
                sorted_isbns = sorted(borrowed_counts.items(), key=lambda x: x[1][0], reverse=True)
            else:
                sorted_isbns = sorted(borrowed_counts.items(), key=lambda x: x[1][0], reverse=False)
            # 构建排序后的书单，但每个ISBN只取一本书（这里取列表中的第一本）
            book_list = [book for _, (_, book) in sorted_isbns]

            return (True, book_list)

        if sort_type == "record_date_up":
            # 根据 record_time 字段对图书进行排序
            # 使用 lambda 函数将 record_time 字符串转换为 datetime 对象进行比较
            book_list = sorted(books,
                               key=lambda book: datetime.strptime(book.record_time, "%Y.%m.%d %H:%M:%S").timestamp(),
                               reverse=True)
            return (True, book_list)

        if sort_type == "num_up" or sort_type == "num_down":
            # 使用字典来统计每个ISBN的数量，并同时存储对应的书籍列表
            isbn_counts = {}
            for book in books:
                isbn = book.ISBN
                if isbn in isbn_counts:
                    isbn_counts[isbn][0] += 1
                    isbn_counts[isbn][1] = book
                else:
                    isbn_counts[isbn] = [1, book]

            # 根据数量对ISBN进行排序（升序）
            if sort_type == "num_up":
                sorted_isbns = sorted(isbn_counts.items(), key=lambda x: x[1][0], reverse=True)
            else:
                sorted_isbns = sorted(isbn_counts.items(), key=lambda x: x[1][0], reverse=False)
            # 构建排序后的书单，但每个ISBN只取一本书（这里取列表中的第一本）
            book_list = [book for _, (_, book) in sorted_isbns]

            return (True, book_list)

        if sort_type == "borrowed" or sort_type == "un_borrowed":
            book_list = []
            if sort_type == "borrowed":
                for book in books:
                    if book.borrowed_by:
                        book_list.append(book)
            else:
                for book in books:
                    if book.borrowed_by == "":
                        book_list.append(book)
            return (True, book_list)

        return (False, f"排序类型 {sort_type} 不存在")

    def _set_calculate_tag(self):
        module_logger.info("线程状态激活")
        self.calculate_1_tag = True
        self.calculate_2_tag = True
        self.calculate_3_tag = True

        if self.thread_1 and self.thread_1.isRunning():
            self.thread_1.quit()
            self.thread_1.wait()
        if self.thread_2 and self.thread_2.isRunning():
            self.thread_2.quit()
            self.thread_2.wait()
        if self.thread_3 and self.thread_3.isRunning():
            self.thread_3.quit()
            self.thread_3.wait()


class Worker(QObject):
    """
    @ClassName：Worker
    @Description：
    负责实现 图书信息统计算法，并返回结果，包括借阅情况，图书种类，出版时间等信息
    @Author：锦沐Python
    """
    # 结束运算信号，用于通知 StatsService 反转线程标志
    finally_signal = pyqtSignal()
    # 数据传输信号，将结果传递给视图层 定义的槽函数 accept_data_func
    data_signal = pyqtSignal(tuple)

    def __init__(self, keywords="", user_id="", book_id=""):
        super().__init__()
        self.keywords = keywords
        self.user_id = user_id
        self.book_id = book_id

    def calculate_books_by_keywords(self):
        """
        查询书籍信息
        @return:
        """
        module_logger.info("线程开始计算")
        (flag, book_list) = RepositoryHelper.get_book_repository().get_books(-1)
        if flag is False:
            msg = book_list
            self.data_signal.emit((False, msg))
            self.finally_signal.emit()
            module_logger.info("线程无法获取图书信息")
            return
        # 截取前50条数据，防止渲染卡顿
        book_num = 50
        filter_books = []
        # 用于返回当前用户已借阅书籍
        borrowed_books = []
        # 定义一个包含需要检查字段的集合
        fields_to_check = ["book_name", "book_id", "category", "author"]
        for book in book_list:
            if self.user_id != "" and book.borrowed_by == self.user_id:
                borrowed_books.append(book)
                continue

            if any(self.keywords in getattr(book, field, "") for field in fields_to_check):
                filter_books.append(book)

        random.shuffle(filter_books)
        all_books = borrowed_books + filter_books[:book_num]

        self.data_signal.emit((True, all_books))
        self.finally_signal.emit()
        module_logger.info("图书匹配完成计算")

    def calculate_borrow_book(self):
        """
        查询书籍信息
        @return:
        """
        module_logger.info("线程开始计算")
        (flag, book_list) = RepositoryHelper.get_book_repository().get_books(-1)
        if flag is False:
            msg = book_list
            self.data_signal.emit((False, msg))
            self.finally_signal.emit()
            module_logger.info("线程无法获取图书信息")
            return

        borrow_book = Book()
        borrow = False

        # 遍历查找
        for book in book_list:
            if book.book_id == self.book_id and book.borrowed_by == "":
                borrow_book = copy.deepcopy(book)
                borrow_book.borrowed_by = self.user_id
                borrow_book.borrow_time = datetime.now().strftime("%Y.%m.%d %H:%M:%S")
                # 设置归还时间（假设归还期限为借阅时间的10天后）
                borrow_time_dt = datetime.strptime(borrow_book.borrow_time, "%Y.%m.%d %H:%M:%S") + timedelta(days=borrow_book.loan_period)
                borrow_book.due_time = borrow_time_dt.strftime("%Y.%m.%d %H:%M:%S")
                borrow = True
                break

        if borrow is False:
            self.data_signal.emit((False, "借阅失败，该书不存在或者已被借阅"))
            self.finally_signal.emit()
            return

        (flag, msg) = RepositoryHelper.get_book_repository().update_book(borrow_book)

        self.data_signal.emit((flag, msg))
        self.finally_signal.emit()
        module_logger.info("图书借阅完成")

    def calculate_back_book(self):
        """
        查询书籍信息
        @return:
        """
        module_logger.info("线程开始计算")
        (flag, book_list) = RepositoryHelper.get_book_repository().get_books(-1)
        if flag is False:
            msg = book_list
            self.data_signal.emit((False, msg))
            self.finally_signal.emit()
            module_logger.info("线程无法获取图书信息")
            return

        backed = False
        back_book = Book()
        # 遍历查找
        for book in book_list:
            if book.book_id == self.book_id and book.borrowed_by == self.user_id:
                back_book = copy.deepcopy(book)
                back_book.borrowed_by = ""
                back_book.borrow_time = ""
                back_book.due_time = ""
                backed = True
                break

        if backed is False:
            self.data_signal.emit((False, "归还书籍失败"))
            self.finally_signal.emit()
            return

        (flag, msg) = RepositoryHelper.get_book_repository().update_book(back_book)
        self.data_signal.emit((flag, msg))
        self.finally_signal.emit()
        module_logger.info("图书归还完成")
