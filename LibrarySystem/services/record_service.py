# -*- coding:utf-8 -*-
"""
图书录入
"""
import logging

from LibrarySystem.services.interface.record_service_interface import RecordServiceInterface
from LibrarySystem.repositories.json.RepositoryHelper import RepositoryHelper
from LibrarySystem.repositories.json.models.book_models import Book

module_logger = logging.getLogger("logger")


class RecordService(RecordServiceInterface):
    """
    @ClassName：RecordService
    @Description： 继承 记录业务层接口类，本项目只创建一个实例
    负责实现添加书籍信息
    @Author：锦沐Python
    """
    def record_book(self, book: Book):
        """
        添加书籍到数据库
        :param book:
        """
        # 存入数据库, 失败返回 False
        (flag, msg) = RepositoryHelper.get_book_repository().add_book(book)
        return (flag, msg)
