# -*- coding:utf-8 -*-
"""
业务层更新功能
"""
import logging

from LibrarySystem.services.interface.update_service_interface import UpdateServiceInterface
from LibrarySystem.repositories.json.models.book_models import Book
from LibrarySystem.repositories.json.RepositoryHelper import RepositoryHelper

module_logger = logging.getLogger("logger")


class UpdateService(UpdateServiceInterface):
    """
    @ClassName：UpdateService
    @Description：
    负责实现 图书信息更新
    @Author：锦沐Python
    """

    def get_book_by_id(self, book_id: str):
        (flag, book) = RepositoryHelper.get_book_repository().query_book(book_id)
        return (flag, book)

    def del_book_by_id(self, book_id: str):
        (flag, msg) = RepositoryHelper.get_book_repository().del_book(book_id)
        return (flag, msg)

    def update_book_by_id(self, book: Book):
        (flag, msg) = RepositoryHelper.get_book_repository().update_book(book)
        return (flag, msg)
