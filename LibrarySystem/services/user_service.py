# -*- coding:utf-8 -*-
"""
用户业务层
"""
import logging

from LibrarySystem.services.interface.user_service_interface import UserServiceInterface
from LibrarySystem.repositories.json.models.user_models import User
from LibrarySystem.repositories.json.RepositoryHelper import RepositoryHelper

module_logger = logging.getLogger("logger")


class UserService(UserServiceInterface):
    """
    @ClassName：UserService
    @Description： 继承用户业务层接口类，本项目只创建一个实例
    负责实现接口功能
    @Author：锦沐Python
    """

    def get_user_by_id(self, user_id):
        (flag, user) = RepositoryHelper.get_user_repository().query_user(user_id)
        if user.account_status == 0:
            return (True, user)
        return (False, "用户账号禁用无法登录")

    def update_user_by_id(self, user: User):
        (flag, msg) = RepositoryHelper.get_user_repository().update_user(user)

        return (flag, msg)

    def del_user_by_id(self, user_id: str):
        (flag, msg) = RepositoryHelper.get_user_repository().del_user(user_id)
        return (flag, msg)

    def add_user(self, user: User):
        (flag, msg) = RepositoryHelper.get_user_repository().add_user(user)
        return (flag, msg)

    def user_password_verification(self, user_id: str, password: str):
        """
        查询用户，并将输入密码与原始密码进行加密比对
       :param user_id:
       :param password:
       :return:
       """
        (flag, user_from_db) = RepositoryHelper.get_user_repository().query_user(user_id)

        if flag is False:
            mag = user_from_db
            return (False, mag)

        result = user_from_db.verify_password(password)

        if result:
            return (True, "密码效验成功")
        return (False, "密码效验失败")
