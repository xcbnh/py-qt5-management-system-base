# -*- coding:utf-8 -*-
import pytest
from LibrarySystem.repositories.json.models.user_models import User
from LibrarySystem.repositories.json.models.book_models import Book

# 定义一个分组标记
@pytest.mark.user_test
@pytest.mark.parametrize("user_id, user_name, email, role, password, account_status, create_time",
                         [(
                            "234567dsfg",  # user_id
                            "john_doe",  # user_name
                            "john.doe@example.com",  # email
                            "user",  # role
                            "SecurePassword123",  # password
                            1,  # account_status (假设1表示激活)
                            "2023-04-01 12:00:00"  # create_time
                        ),
                        (
                            "A001",  # user_id（但这里可能是管理员ID）
                            "admin_user",  # user_name
                            "admin@example.com",  # email
                            "admin",  # role
                            "SuperSecurePass",  # password
                            1,  # account_status
                            "2023-03-15 09:00:00"  # create_time
                        ),
                        (
                            "234567dsfg",  # user_id
                            "jane_doe",  # user_name
                            "jane.doe@example.com",  # email
                            "user",  # role
                            "JanePassword123",  # password
                            0,  # account_status (假设0表示未激活)
                            "2023-04-02 15:30:00"  # create_time
                        ),
                         (
                            1,  # user_id
                            "invalid_user",  # user_name
                            "invalid-email",  # 无效的email地址
                            "user",  # role
                            "InvalidPass123",  # password
                            1,  # account_status
                            "2023-04-03 08:00:00"  # create_time
                        ),
                         (
                            "dsfghfb",  # user_id
                            "empty_pass_user",  # user_name
                            "empty_pass@example.com",  # email
                            "user",  # role
                            "",  # 空密码
                            1,  # account_status
                            "2023-04-04 10:00:00"  # create_time
                        )])
def test_user(user_id, username, email, role, password,
              account_status, registration_time):
    user = User()
    user.user_id = user_id
    user.username = username
    user.email = email
    user.role = role
    user.password = password
    user.account_status = account_status
    user.create_time = registration_time
    user.obj_to_json()


@pytest.mark.book_test
@pytest.mark.parametrize("book_id, book_name, ISBN, record_time, category, author, publisher, publication_date, borrowed_by, borrow_time, loan_period, due_time",
                         [
                             # 测试用例1：一本新书，未被借阅
                             ("edrfgtyggf", "Python Programming", "978-1234567890", "2023-04-01", "Computer Science",
                              "John Doe", "Wiley", "2023-03-15", None, None, None, None),

                             # 测试用例2：一本旧书，已被借阅
                             ("edrfgtyggf", "The Great Gatsby", "978-0987654321", "2022-05-01", "Literature",
                              "F. Scott Fitzgerald", "Simon & Schuster", "1925-04-10", "Jane Doe",
                              "2023-04-05 14:30:00", 14, "2023-04-19 14:30:00"),

                             # 测试用例3：一本电子书，无ISBN（假设使用其他标识符），未被借阅
                             ("edrfgtyggf", "Learning Python", "eBook-12345", "2023-04-02", "Education", "Jane Smith",
                              "O'Reilly Media", "2023-01-01", None, None, None, None),

                             # 测试用例4：一本即将到期的借阅书籍
                             ("sd", "1984", "978-1471125310", "2023-03-15", "Dystopian", "George Orwell",
                              "Penguin Books", "1949-06-08", "Alice Johnson", "2023-04-01 08:00:00", 7,
                              "2023-04-08 08:00:00"),

                             # 测试用例5：一本书籍，所有字段均为示例值
                             (5, "Example Book", "978-XXXXXXXXXX", "2023-04-03", "Example Category",
                              "Example Author", "Example Publisher", "2023-04-01", "Example Borrower",
                              "2023-04-02 13:00:00", 30, "2023-05-02 13:00:00")
                         ])
def test_book(book_id, title, ISBN, entry_time, category,
              author, publisher, publication_date, borrowed_by,
              borrow_time, loan_period, due_time):
    book = Book()
    book.book_id = book_id
    book.title = title
    book.ISBN = ISBN
    book.entry_time = entry_time
    book.category = category
    book.author = author
    book.publisher = publisher
    book.publication_date = publication_date
    book.borrowed_by = borrowed_by
    book.borrow_time = borrow_time
    book.loan_period = loan_period
    book.due_time = due_time
    book.obj_to_json()