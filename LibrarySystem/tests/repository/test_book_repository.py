# -*- coding:utf-8 -*-
import pytest

from LibrarySystem.repositories.json.models.book_models import Book
from LibrarySystem.repositories.json.book_repository import BookRepository

@pytest.mark.book_repository_test
@pytest.mark.parametrize("book_id, book_name, ISBN,record_time, category, author,publisher, publication_date, borrowed_by,borrow_time, loan_period, due_time",
                         [(  
                            "book001",  # book_id  
                            "Python Programming",  # book_name
                            "978-1234567890",  # ISBN  
                            "2023-01-01",  # record_time
                            "Computer Science",  # category  
                            "John Doe",  # author  
                            "Wiley",  # publisher  
                            "2022-05-01",  # publication_date  
                            None,  # borrowed_by (未被借出)  
                            None,  # borrow_time (未被借出)  
                            None,  # loan_period (未被借出)  
                            None,  # due_time (未被借出)  
                        ),  
                        (  
                            "book002",  
                            "Machine Learning Basics",  
                            "978-0987654321",  
                            "2023-01-02",  
                            "Mathematics",  
                            "Jane Smith",  
                            "O'Reilly",  
                            "2021-10-15",  
                            "Alice Johnson",  # borrowed_by  
                            "2023-01-10",  # borrow_time  
                            30,  # loan_period  
                            "2023-02-09",  # due_time  
                        )])
def test_book(book_id, title, ISBN,entry_time, category, author,
                 publisher, publication_date, borrowed_by,
                 borrow_time, loan_period, due_time):
    book = Book(book_id, title, ISBN,entry_time, category, author,
                 publisher, publication_date, borrowed_by,
                 borrow_time, loan_period, due_time)
    book_rep = BookRepository()
    assert book_rep.add_book(book) == False
    assert book_rep.update_book(book) != False
    result_3 = book_rep.query_book(book_id)
    assert result_3 != False

    assert book_rep.del_book(book_id) != False

    print("result_3"+str(result_3.obj_to_dict()))