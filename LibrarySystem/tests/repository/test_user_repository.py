# -*- coding:utf-8 -*-
import pytest

from LibrarySystem.repositories.json.models.user_models import User
from LibrarySystem.repositories.json.user_repository import UserRepository
# from LibrarySystem.repositories.database.json.book_repository import BookRepository

@pytest.mark.user_repository_test
@pytest.mark.parametrize("user_id, user_name, email, role, password, account_status, create_time",
                         [(
                            "fdghj",  # user_id
                            "john_doe",  # user_name
                            "john.doe@example.com",  # email
                            "user",  # role
                            "SecurePassword123",  # password
                            1,  # account_status (假设1表示激活)
                            "2023-04-01 12:00:00"  # create_time
                        ),
                        (
                            "dgfhjrge",  # user_id（但这里可能是管理员ID）
                            "admin_user",  # user_name
                            "admin@example.com",  # email
                            "admin",  # role
                            "SuperSecurePass",  # password
                            0,  # account_status
                            "456789"  # create_time
                        ),])
def test_user(user_id, username, email, role, password,
              account_status, registration_time):
    user = User(user_id, username, email, role, password, account_status, registration_time)
    user_rep = UserRepository()
    assert user_rep.add_user(user) == False
    assert user_rep.update_user(user) != False
    result_3 = user_rep.query_user(user_id)
    assert result_3 != False
    assert user_rep.del_user(user_id) != False

    print("result_3"+str(result_3.obj_to_dict()))