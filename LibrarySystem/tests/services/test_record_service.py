# -*- coding:utf-8 -*-
import pytest

from LibrarySystem.services.record_service import RecordService
from LibrarySystem.repositories.json.models.book_models import Book
record_service = RecordService()
@pytest.mark.record_test
@pytest.mark.parametrize("book_id, book_name, ISBN, record_time, category,"
                         "author, publisher, publication_date, borrowed_by,"
                         "borrow_time, loan_period, due_time",[

                             ("edrfgtyggf", "Python Programming", "978-1234567890", "2023-04-01", "Computer Science",
                              "John Doe", "Wiley", "2023-03-15", None, None, None, None),

                             # 测试用例2：一本旧书，已被借阅
                             ("edrfgtyggf", "The Great Gatsby", "978-0987654321", "2022-05-01", "Literature",
                              "F. Scott Fitzgerald", "Simon & Schuster", "1925-04-10", "Jane Doe",
                              "2023-04-05 14:30:00", 14, "2023-04-19 14:30:00"),

                             # 测试用例3：一本电子书，无ISBN（假设使用其他标识符），未被借阅
                             ("edrfgtyggf", "Learning Python", "eBook-12345", "2023-04-02", "Education", "Jane Smith",
                              "O'Reilly Media", "2023-01-01", None, None, None, None),

                             # 测试用例4：一本即将到期的借阅书籍
                             ("sd", "1984", "978-1471125310", "2023-03-15", "Dystopian", "George Orwell",
                              "Penguin Books", "1949-06-08", "Alice Johnson", "2023-04-01 08:00:00", 7,
                              "2023-04-08 08:00:00"),

                             # 测试用例5：一本书籍，所有字段均为示例值
                             ("efrthyjuthref", "Example Book", "978-XXXXXXXXXX", "2023-04-03", "Example Category",
                              "Example Author", "Example Publisher", "2023-04-01", "Example Borrower",
                              "2023-04-02 13:00:00", 30, "2023-05-02 13:00:00")

                        ])
def test_record(book_id, title, ISBN,entry_time, category, author,
                 publisher, publication_date, borrowed_by,
                 borrow_time, loan_period, due_time):
    book = Book(book_id, title, ISBN,entry_time, category, author,
                 publisher, publication_date, borrowed_by,
                 borrow_time, loan_period, due_time)
    result = record_service.record_book(book)
    # 所有书籍已存在，返回False
    assert result == False