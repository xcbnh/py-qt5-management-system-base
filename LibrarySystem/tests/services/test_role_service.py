# -*- coding:utf-8 -*-
import pytest

# from LibrarySystem.services.role_service import requires_auth


# @requires_auth(["user", "admin"])
# def fun1(x, y):
#     print(x+y)
#
#
# @requires_auth(["admin"])
# def fun2(x, y):
#     print(x - y)
#
#
# @pytest.mark.role_test
# def test_role():
#     assert fun1(1,2) != False
#     assert fun2(3,5) == False