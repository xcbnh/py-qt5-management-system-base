# -*- coding:utf-8 -*-
import pytest
import matplotlib
import matplotlib.pyplot as plt
from LibrarySystem.services.stats_service import StatsService

# 设置matplotlib正常显示中文和负号
matplotlib.rcParams['font.sans-serif'] = ['SimHei']  # 用黑体显示中文
matplotlib.rcParams['axes.unicode_minus'] = False  # 正常显示负号

stats_service = StatsService()
fig, ax = plt.subplots()
@pytest.mark.stats_test
def test_stats():
    # (book_name_list, book_num_list) = stats_service.get_book_hot_case()
    # ax.plot(book_name_list, book_num_list)  # Plot some data on the Axes.
    # plt.show()

    # sizes = stats_service.get_book_total_case()
    # labels = ("总数", "借阅数", "剩余数")
    # ax.pie(sizes, labels=labels)  # Plot some data on the Axes.
    # plt.show()

    # (time_list, book_name_list) = stats_service.get_book_history_case()
    # bars = ax.bar(time_list, [1]*len(time_list))  # Plot some data on the Axes.
    # # 为每个条形添加书名标签
    # for bar, name in zip(bars, book_name_list):
    #     height = bar.get_height()  # 获取条形的高度（这里都是1）
    #     ax.annotate(name,
    #                 xy=(bar.get_x() + bar.get_width() / 2, height),  # 将标签放在条形的正中央上方
    #                 xytext=(0, 3),  # 文本相对于指定点的偏移
    #                 textcoords="offset points",
    #                 ha='center', va='bottom',
    #                 rotation=90)  # 水平居中对齐，垂直底部对齐（但实际上是向上偏移）
    # ax.set_ylim(0, 5)
    # plt.show()

    # (category_list, book_num) = stats_service.get_book_categories_case()
    # ax.pie(book_num, labels=category_list)  # Plot some data on the Axes.
    # plt.show()

    # (category_list, book_num) = stats_service.get_book_user_categories_case(user_id="456789899")
    # ax.pie(book_num, labels=category_list)  # Plot some data on the Axes.
    # plt.show()

    (time_list, book_name_list) = stats_service.get_book_user_history_case(user_id="456789899")
    bars = ax.bar(time_list, [1]*len(time_list))  # Plot some data on the Axes.
    # 为每个条形添加书名标签
    for bar, name in zip(bars, book_name_list):
        height = bar.get_height()  # 获取条形的高度（这里都是1）
        ax.annotate(name,
                    xy=(bar.get_x() + bar.get_width() / 2, height),  # 将标签放在条形的正中央上方
                    xytext=(0, 3),  # 文本相对于指定点的偏移
                    textcoords="offset points",
                    ha='center', va='bottom',
                    rotation=90)  # 水平居中对齐，垂直底部对齐（但实际上是向上偏移）
    ax.set_ylim(0, 5)
    plt.show()