# -*- coding:utf-8 -*-
import pytest

from LibrarySystem.services.update_service import UpdateService
from LibrarySystem.repositories.json.models.book_models import Book
update_service = UpdateService()
@pytest.mark.record_test
@pytest.mark.parametrize("book_id, book_name, ISBN, record_time, category,"
                         "author, publisher, publication_date, borrowed_by,"
                         "borrow_time, loan_period, due_time",[
                             ("345678976543", "Python Programming", "978-1234567890", "2023-04-01", "Computer Science",
                              "John Doe", "Wiley", "2023-03-15", "Alice Johnson", "2023-04-01 08:00:00", 7,
                              "2023-04-08 08:00:00"),
                             # 测试用例2：一本旧书，已被借阅
                             ("sdfgh", "The  Gatsby", "978-0987654321", "2022-05-01", "Literature",
                              "F. Scott Fitzgerald", "Simon & Schuster", "1925-04-10", "Jane Doe",
                              "2023-04-05", 14, "2023-04-19 14:30:00")
                        ])
def test_record(book_id, title, ISBN,entry_time, category, author,
                 publisher, publication_date, borrowed_by,
                 borrow_time, loan_period, due_time):
    book = Book(book_id, title, ISBN,entry_time, category, author,
                 publisher, publication_date, borrowed_by,
                 borrow_time, loan_period, due_time)

    # result = update_service.update_book_by_id(book)
    # assert result != False

    result = update_service.del_book_by_id(book_id)
    assert result != False