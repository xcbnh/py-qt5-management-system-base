# -*- coding:utf-8 -*-
import pytest

from LibrarySystem.repositories.json.models.user_models import User
from LibrarySystem.services.user_service import UserService

user_service = UserService()
@pytest.mark.user_repository_test
@pytest.mark.parametrize("user_id, user_name, email, role, password, account_status, create_time",
                         [(
                            "TEST1",  # user_id
                            "john_doe",  # user_name
                            "john.doe@example.com",  # email
                            "user",  # role
                            "SecurePassword",  # password
                            1,  # account_status (假设1表示激活)
                            "2023-04-01 12:00:00"  # create_time
                        ),
                        (
                            "TEST2",  # user_id（但这里可能是管理员ID）
                            "admin_user",  # user_name
                            "admin@example.com",  # email
                            "admin",  # role
                            "SecurePassword",  # password
                            1234,  # account_status
                            "456789"  # create_time
                        ),])
def test_user(user_id, username, email, role, password,
              account_status, registration_time):
    user = User(user_id, username, email, role, password, account_status, registration_time)
    # user_service.add_user(user)

    # user_service.update_user_by_id(user)
    #
    # result = user_service.user_password_verification(user)
    # assert result != False
    user_service.del_user_by_id(user_id)

