"""
@FileName：tets.py\n
@Description：\n
@Author：锦沐Python\n
@Time：2024/7/26 17:26\n
"""
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as mcolors

def generate_wordcloud(words, max_words=50):
    num_words = min(len(words), max_words)
    x = np.random.rand(num_words)
    y = np.random.rand(num_words)
    sizes = np.random.randint(8, 30, num_words)  # 随机生成字体大小

    # 随机生成颜色
    colors = [list(mcolors.CSS4_COLORS.values())[i] for i in np.random.randint(0, len(mcolors.CSS4_COLORS), num_words)]


    for i in range(num_words):
        plt.text(x[i], y[i], words[i], fontsize=sizes[i], color=colors[i], ha='center', va='center')

    plt.xlim(0, 1)
    plt.ylim(0, 1)
    plt.axis('off')  # 关闭坐标轴
    plt.show()

# 示例词语列表
words = ["apple", "orange", "banana", "grape", "peach", "pear", "watermelon", "strawberry", "pineapple"]

generate_wordcloud(words)
