# -*- coding:utf-8 -*-
"""
提示窗 组件
"""
from datetime import datetime

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog

from LibrarySystem.views.ui.Messager import Ui_Messager
from LibrarySystem.config.config import Config

class Messager(QDialog, Ui_Messager):
    """
    @ClassName：Messager
    @Description：Messager 创建一个弹窗，
    本项目创建的弹窗实例不会被主动删除，因此可以重复使用一个实例，
    负责必要的信息提示
    注意：只能在窗体类内创建实例，其他地方创建会导致无法启动程序
    @Author：锦沐Python
     """
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.config = Config()
        self.setWindowIcon(QIcon(self.config.ICON_PATH))
        # 设置窗口始终在顶部，隐藏问号
        self.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.WindowCloseButtonHint)
        self.time_label.setText(datetime.now().strftime("%Y-%m-%d %H:%M:S"))

    def show_info(self, msg: str):
        self.msg_label.setStyleSheet("color: blue;")
        self.msg_label.setText(str(msg))
        self.show()

    def show_warn(self, msg: str):
        self.msg_label.setStyleSheet("color: yellow;")
        self.msg_label.setText(str(msg))
        self.show()

    def show_error(self, msg: str):
        self.msg_label.setStyleSheet("color: red;")
        self.msg_label.setText(str(msg))
        self.show()
