# -*- coding: utf-8 -*-
"""
除了多线程信号以外，项目所有信号都在这里管理
"""
from PyQt5.QtCore import QObject, pyqtSignal


class QSignals(QObject):
    """
    @ClassName：QSignals
    @Description：本项目只创建一个 QSignals 信号实例，
    负责程序内所有信号，提供信号给需要的地方进行接收和发送
    注意：定义的信号 不要使用self.信号名，该写法不对，不能被QObject管理
    @Author：锦沐Python
    """
    _instance = None

    # 从登录界面跳转至管理界面
    LoginToAdmin_Signal = pyqtSignal()

    # 从管理界面跳转至登录界面
    AdminToLogin_Signal = pyqtSignal()

    # 发送给 AdminWidget 的信号，用于告知管理页获取 登录的用户信息
    LoginUserInfo_Signal = pyqtSignal()

    # 书籍列表更新
    ListWidgetRefrsh_Signal = pyqtSignal()

    # 如果你需要传参数,请使用参数类型不要用参数名
    # exp_Signal = pyqtSignal(str, int)

    def __init__(self):
        super().__init__()

    def __new__(cls):
        # __new__ 方法确保只创建一个实例
        if not cls._instance:
            cls._instance = super().__new__(cls)
        return cls._instance


# 本项目引用该对象进行配置信号的发送与接收
# q_signals.LoginUserInfo_Signal.emit() 发送
#  q_signals.LoginUserInfo_Signal.connect(槽函数) 接收
q_signals = QSignals()
