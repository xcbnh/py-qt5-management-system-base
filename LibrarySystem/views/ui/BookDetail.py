# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'BookDetail.ui'
#
# Created by: PyQt5 UI code generator 5.15.9
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_BookDetail(object):
    def setupUi(self, BookDetail):
        BookDetail.setObjectName("BookDetail")
        BookDetail.resize(700, 130)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(BookDetail.sizePolicy().hasHeightForWidth())
        BookDetail.setSizePolicy(sizePolicy)
        BookDetail.setMinimumSize(QtCore.QSize(700, 130))
        BookDetail.setMaximumSize(QtCore.QSize(16777215, 145))
        BookDetail.setLayoutDirection(QtCore.Qt.LeftToRight)
        BookDetail.setStyleSheet("* { \n"
"    font-family:\"阿里巴巴普惠体\";\n"
" }\n"
"\n"
"QWidget{\n"
"background-color: rgb(255, 255, 255);\n"
"\n"
"}\n"
"#img_label{\n"
"    border-image: url(:/icon_/example_book.png);\n"
"}\n"
"\n"
"\n"
"#borrow_book_button{\n"
"    background-color: rgb(0, 170, 255);\n"
"    border-radius: 5px; /*设置圆角半径 */\n"
"    color: rgb(255, 255, 255);\n"
"}\n"
"#back_book_button{\n"
"    background-color: rgb(0, 0, 0);\n"
"    border-radius: 5px; /*设置圆角半径 */\n"
"    color: rgb(255, 255, 255);\n"
"}\n"
"#BookDetail{\n"
"\n"
"     border: 4px solid rgb(77, 77, 77);\n"
"    border-radius: 5px; /*设置圆角半径 */\n"
"}")
        self.horizontalLayout = QtWidgets.QHBoxLayout(BookDetail)
        self.horizontalLayout.setContentsMargins(2, 2, 2, 2)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.widget_13 = QtWidgets.QWidget(BookDetail)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget_13.sizePolicy().hasHeightForWidth())
        self.widget_13.setSizePolicy(sizePolicy)
        self.widget_13.setMaximumSize(QtCore.QSize(16777215, 150))
        self.widget_13.setObjectName("widget_13")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.widget_13)
        self.horizontalLayout_2.setContentsMargins(3, 3, 3, 3)
        self.horizontalLayout_2.setSpacing(3)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.img_label = QtWidgets.QLabel(self.widget_13)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.img_label.sizePolicy().hasHeightForWidth())
        self.img_label.setSizePolicy(sizePolicy)
        self.img_label.setMinimumSize(QtCore.QSize(80, 100))
        self.img_label.setMaximumSize(QtCore.QSize(80, 100))
        self.img_label.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.img_label.setText("")
        self.img_label.setAlignment(QtCore.Qt.AlignCenter)
        self.img_label.setObjectName("img_label")
        self.horizontalLayout_2.addWidget(self.img_label)
        self.horizontalLayout.addWidget(self.widget_13)
        self.widget_14 = QtWidgets.QWidget(BookDetail)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget_14.sizePolicy().hasHeightForWidth())
        self.widget_14.setSizePolicy(sizePolicy)
        self.widget_14.setMinimumSize(QtCore.QSize(0, 100))
        self.widget_14.setMaximumSize(QtCore.QSize(16777215, 150))
        self.widget_14.setObjectName("widget_14")
        self.verticalLayout_13 = QtWidgets.QVBoxLayout(self.widget_14)
        self.verticalLayout_13.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_13.setSpacing(0)
        self.verticalLayout_13.setObjectName("verticalLayout_13")
        self.book_detail_widget_1 = QtWidgets.QWidget(self.widget_14)
        self.book_detail_widget_1.setObjectName("book_detail_widget_1")
        self.horizontalLayout_13 = QtWidgets.QHBoxLayout(self.book_detail_widget_1)
        self.horizontalLayout_13.setContentsMargins(2, 2, 2, 2)
        self.horizontalLayout_13.setSpacing(5)
        self.horizontalLayout_13.setObjectName("horizontalLayout_13")
        self.book_name_label = QtWidgets.QLabel(self.book_detail_widget_1)
        font = QtGui.QFont()
        font.setFamily("阿里巴巴普惠体")
        font.setPointSize(12)
        self.book_name_label.setFont(font)
        self.book_name_label.setObjectName("book_name_label")
        self.horizontalLayout_13.addWidget(self.book_name_label)
        self.book_category_label = QtWidgets.QLabel(self.book_detail_widget_1)
        font = QtGui.QFont()
        font.setFamily("阿里巴巴普惠体")
        font.setPointSize(8)
        self.book_category_label.setFont(font)
        self.book_category_label.setObjectName("book_category_label")
        self.horizontalLayout_13.addWidget(self.book_category_label)
        self.book_id_label = QtWidgets.QLabel(self.book_detail_widget_1)
        font = QtGui.QFont()
        font.setFamily("阿里巴巴普惠体")
        font.setPointSize(8)
        self.book_id_label.setFont(font)
        self.book_id_label.setObjectName("book_id_label")
        self.horizontalLayout_13.addWidget(self.book_id_label)
        self.verticalLayout_13.addWidget(self.book_detail_widget_1)
        self.book_detail_widget_2 = QtWidgets.QWidget(self.widget_14)
        self.book_detail_widget_2.setObjectName("book_detail_widget_2")
        self.horizontalLayout_14 = QtWidgets.QHBoxLayout(self.book_detail_widget_2)
        self.horizontalLayout_14.setContentsMargins(2, 2, 2, 2)
        self.horizontalLayout_14.setSpacing(5)
        self.horizontalLayout_14.setObjectName("horizontalLayout_14")
        self.author_label = QtWidgets.QLabel(self.book_detail_widget_2)
        font = QtGui.QFont()
        font.setFamily("阿里巴巴普惠体")
        font.setPointSize(8)
        self.author_label.setFont(font)
        self.author_label.setObjectName("author_label")
        self.horizontalLayout_14.addWidget(self.author_label)
        self.publisher_label = QtWidgets.QLabel(self.book_detail_widget_2)
        font = QtGui.QFont()
        font.setFamily("阿里巴巴普惠体")
        font.setPointSize(8)
        self.publisher_label.setFont(font)
        self.publisher_label.setObjectName("publisher_label")
        self.horizontalLayout_14.addWidget(self.publisher_label)
        self.record_time_label = QtWidgets.QLabel(self.book_detail_widget_2)
        font = QtGui.QFont()
        font.setFamily("阿里巴巴普惠体")
        font.setPointSize(8)
        self.record_time_label.setFont(font)
        self.record_time_label.setObjectName("record_time_label")
        self.horizontalLayout_14.addWidget(self.record_time_label)
        self.verticalLayout_13.addWidget(self.book_detail_widget_2)
        self.book_detail_widget_3 = QtWidgets.QWidget(self.widget_14)
        self.book_detail_widget_3.setObjectName("book_detail_widget_3")
        self.horizontalLayout_15 = QtWidgets.QHBoxLayout(self.book_detail_widget_3)
        self.horizontalLayout_15.setContentsMargins(2, 2, 2, 2)
        self.horizontalLayout_15.setSpacing(5)
        self.horizontalLayout_15.setObjectName("horizontalLayout_15")
        self.ISBN_label = QtWidgets.QLabel(self.book_detail_widget_3)
        font = QtGui.QFont()
        font.setFamily("阿里巴巴普惠体")
        font.setPointSize(8)
        self.ISBN_label.setFont(font)
        self.ISBN_label.setObjectName("ISBN_label")
        self.horizontalLayout_15.addWidget(self.ISBN_label)
        self.publication_date_label = QtWidgets.QLabel(self.book_detail_widget_3)
        font = QtGui.QFont()
        font.setFamily("阿里巴巴普惠体")
        font.setPointSize(8)
        self.publication_date_label.setFont(font)
        self.publication_date_label.setObjectName("publication_date_label")
        self.horizontalLayout_15.addWidget(self.publication_date_label)
        self.rent_status_label = QtWidgets.QLabel(self.book_detail_widget_3)
        font = QtGui.QFont()
        font.setFamily("阿里巴巴普惠体")
        font.setPointSize(8)
        self.rent_status_label.setFont(font)
        self.rent_status_label.setObjectName("rent_status_label")
        self.horizontalLayout_15.addWidget(self.rent_status_label)
        self.verticalLayout_13.addWidget(self.book_detail_widget_3)
        self.book_detail_widget_4 = QtWidgets.QWidget(self.widget_14)
        self.book_detail_widget_4.setObjectName("book_detail_widget_4")
        self.horizontalLayout_16 = QtWidgets.QHBoxLayout(self.book_detail_widget_4)
        self.horizontalLayout_16.setContentsMargins(2, 2, 2, 2)
        self.horizontalLayout_16.setSpacing(5)
        self.horizontalLayout_16.setObjectName("horizontalLayout_16")
        self.borrowed_by_label = QtWidgets.QLabel(self.book_detail_widget_4)
        font = QtGui.QFont()
        font.setFamily("阿里巴巴普惠体")
        font.setPointSize(8)
        self.borrowed_by_label.setFont(font)
        self.borrowed_by_label.setObjectName("borrowed_by_label")
        self.horizontalLayout_16.addWidget(self.borrowed_by_label)
        self.loan_period_label = QtWidgets.QLabel(self.book_detail_widget_4)
        font = QtGui.QFont()
        font.setFamily("阿里巴巴普惠体")
        font.setPointSize(8)
        self.loan_period_label.setFont(font)
        self.loan_period_label.setObjectName("loan_period_label")
        self.horizontalLayout_16.addWidget(self.loan_period_label)
        self.due_time_label = QtWidgets.QLabel(self.book_detail_widget_4)
        font = QtGui.QFont()
        font.setFamily("阿里巴巴普惠体")
        font.setPointSize(8)
        self.due_time_label.setFont(font)
        self.due_time_label.setObjectName("due_time_label")
        self.horizontalLayout_16.addWidget(self.due_time_label)
        self.borrow_book_button = QtWidgets.QPushButton(self.book_detail_widget_4)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.borrow_book_button.sizePolicy().hasHeightForWidth())
        self.borrow_book_button.setSizePolicy(sizePolicy)
        self.borrow_book_button.setMaximumSize(QtCore.QSize(100, 40))
        self.borrow_book_button.setObjectName("borrow_book_button")
        self.horizontalLayout_16.addWidget(self.borrow_book_button)
        self.back_book_button = QtWidgets.QPushButton(self.book_detail_widget_4)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.back_book_button.sizePolicy().hasHeightForWidth())
        self.back_book_button.setSizePolicy(sizePolicy)
        self.back_book_button.setMaximumSize(QtCore.QSize(100, 40))
        self.back_book_button.setObjectName("back_book_button")
        self.horizontalLayout_16.addWidget(self.back_book_button)
        self.verticalLayout_13.addWidget(self.book_detail_widget_4)
        self.horizontalLayout.addWidget(self.widget_14)

        self.retranslateUi(BookDetail)
        QtCore.QMetaObject.connectSlotsByName(BookDetail)

    def retranslateUi(self, BookDetail):
        _translate = QtCore.QCoreApplication.translate
        BookDetail.setWindowTitle(_translate("BookDetail", "Form"))
        self.book_name_label.setText(_translate("BookDetail", "被讨厌的勇气"))
        self.book_category_label.setText(_translate("BookDetail", "文学"))
        self.book_id_label.setText(_translate("BookDetail", "编号：book_wesd3"))
        self.author_label.setText(_translate("BookDetail", "作者：我是的承认"))
        self.publisher_label.setText(_translate("BookDetail", "出版社："))
        self.record_time_label.setText(_translate("BookDetail", "录入时间："))
        self.ISBN_label.setText(_translate("BookDetail", "ISBN："))
        self.publication_date_label.setText(_translate("BookDetail", "出版时间："))
        self.rent_status_label.setText(_translate("BookDetail", "借阅状态："))
        self.borrowed_by_label.setText(_translate("BookDetail", "借阅人："))
        self.loan_period_label.setText(_translate("BookDetail", "可借天数："))
        self.due_time_label.setText(_translate("BookDetail", "应还时间："))
        self.borrow_book_button.setText(_translate("BookDetail", "借阅"))
        self.back_book_button.setText(_translate("BookDetail", "归还"))

